<?php

	class Government extends MY_Controller
	{
		private $client;

		function __construct()
		{
			parent::__construct();
			$this->load->model('crud_model');
        	$this->client = new \GuzzleHttp\Client([
            'verify' => false, 
            'referer' => true,
            'headers' => [
                            'User-Agent' => $_SERVER['HTTP_USER_AGENT'],
                            'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
                            'Accept-Encoding' => 'gzip, deflate, br',
                        ],
            ]);
		}

		public function index()
		{
			$sort = null;
			if (isset($_GET['sort'])) {
				$sort = $_GET['sort'];
			}

			$data['sort'] = $sort;

			$lang = $this->get_language();
			$data['title'] = get_lang('government');
			$data['id'] = 'site';
			$data['subtitle'] = 'information';
			$data['data_mode'] = 'general';
			$data['page_heading'] = APP_NAME;
			$data['is_bilingual'] = true;

			$data['breadcrumb'] = [
						[
							'title' => $this->get_lang('home'),
							'url' => '',
							'active' => false
						],
						[
							'title' => $this->get_lang('government'),
							'url' => 'government',
							'active' => true
						]
			];

			//language, section, general info (title, subtitle, order), item (specific content)
				$data['copy'] = json_decode(file_get_contents(base_url('database/json/government/government_page_'.$lang.'.json')));

				$data['meta_data'] = [
					'site_url' => 'government',
					'title' => 'Waste4Change - ' . ucwords(lang('government')),
					'description_1' => $data['copy']->banner->title,
					'description_2' => $data['copy']->banner->title,
					'keywords' => 'pemerintahan, program pemerintah, pemerintah, pemerintah waste4change',
				];
			$data['researchs'] = $this->getData($sort);

			$this->render_page('government/index', $data, 'services');
		}

		public function detail($slug)
		{
			$data['lang'] = $this->get_language();
			$data['title'] = APP_NAME;
			$data['id'] = 'brand';
			$data['subtitle'] = 'information';
			$data['data_mode'] = 'general';
			$data['page_heading'] = 'research';
			$data['is_bilingual'] = true;

			foreach ($this->getDatas() as $research) {
				if ($research['slug'] == $slug) {
					$data['meta_data'] = [
						'site_url' => 'research/' . $slug,
						'title' => 'Waste4Change - ' . $research['hidden_title'][$data['lang']],
						'description_1' => $research['content'][$data['lang']]['detail'],
						'description_2' => $research['content'][$data['lang']]['detail'],
						'keywords' => $research['keyword'][$data['lang']],
						'image' => $research['banner']['thumb_image'][$data['lang']],
					];
				}
			}

			$data['breadcrumb'] =
				[
					[
						'title' => lang('home'),
						'active' => false,
						'url' => W4C_URL
					],
					[
						'title' => lang('research'),
						'active' => false,
						'url' => site_url('research')
					],
					[
						'title' => lang('detail'),
						'active' => true,
						'url' => site_url('research/' . $slug)
					],
				];
			$data['researchs'] = $this->getDatas();
			$data['slug'] = $slug;

			$this->render_page('research/detail', $data, 'services');
		}

		public function getData($sortBy = null)
		{
			$array = $this->getDatas();
			if ($sortBy !== null) {
				if ($sortBy === 'date') {
					$array = $this->array_orderby($array, 'file', SORT_DESC);
				} else {
					$array = $this->array_orderby($array, 'hidden_title', SORT_ASC);
				}
			}

			return $array;
		}

		private function getDatas()
		{
			$res = $this->client->request('GET', site_url(DIR_STATIC_DB . '/research/research.json'));
			return json_decode($res->getBody(), true);
		}

		public function array_orderby()
		{
			$args = func_get_args();
			$data = array_shift($args);
			foreach ($args as $n => $field) {
				if (is_string($field)) {
					$tmp = array();
					foreach ($data as $key => $row)
						$tmp[$key] = $row[$field];
					$args[$n] = $tmp;
				}
			}
			$args[] = &$data;
			call_user_func_array('array_multisort', $args);
			return array_pop($args);
		}
	}		
?>

